﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Перепись
{
    class Program
    {
        static void Main(string[] args)
        {
            int v = 0, vmax = 0, s = 1, i = 0, mem = 0;
            Console.Write("Введите количество жильцов: ");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine("");
            while (i < n)
            {
                Console.Write("Введите возраст жильца: ");
                v = int.Parse(Console.ReadLine());
                Console.Write("Введите пол жильца (0 - женский, 1 - мужской): ");
                s = int.Parse(Console.ReadLine());
                if (s == 1 && vmax < v)
                {
                    vmax = v;
                    mem = i + 1;
                }
                i++;
            }
            if (mem != 0)
            {
                Console.WriteLine($"Старший мужчина указан под номером {mem}");
            }
            else
            {
                Console.WriteLine("-1");
            }
            Console.ReadKey();
        }
    }
}
